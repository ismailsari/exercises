using System.Threading.Tasks;
using DatingApp.API.Data;
using DatingApp.API.DTOs;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace DatingApp.API.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthRepository _repo;
        public AuthController(IAuthRepository repo)
        {
            _repo = repo;
            
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserForRegisterDTO UserForRegisterDTO){
            
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            
            UserForRegisterDTO.UserName = UserForRegisterDTO.UserName.ToLower();
            
            if (await _repo.UserExists(UserForRegisterDTO.UserName))
                return BadRequest("Username is already taken");

            var userToCreate = new User {
                UserName = UserForRegisterDTO.UserName
            };

            var createUser = await _repo.Register(userToCreate,UserForRegisterDTO.Password);

            return StatusCode(201);
        }
    }
}